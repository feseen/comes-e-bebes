<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>

	<?php
	/**
	 * Hook: woocommerce_before_single_product_summary.
	 *
	 * @hooked woocommerce_show_product_sale_flash - 10
	 * @hooked woocommerce_show_product_images - 20
	 */
	//do_action( 'woocommerce_before_single_product_summary' );
	?>
    <?php
        $image = wp_get_attachment_url( $product->image_id );
        add_action("single-product-portions", "woocommerce_template_single_add_to_cart", 20);
        add_action("single-product-portions", "woocommerce_template_single_sharing", 30);
    ?>
	<div class="summary">

        <div class="single-product-img">
            <img src="<?= $image ?>">
        </div>
        <div class="single-product-info">
            <h2 class="single-product-name"><?= $product->name ?></h2>
            <p class="single-product-description"><?= $product->description ?></p>
            <div class="single-product-sale">
                <?php do_action("single-product-portions"); ?>
    
            </div>
        </div>
	</div>
    <?php 
        $category_id = $product->category_ids[0];
        if($category_id){
            $category = get_term_by( 'id', $category_id, 'product_cat' );
            echo "<h1> MAIS COMIDA " . strtoupper($category->name) . "</h1>";
            $args = array(
                'post_type' => 'product',
                'posts_per_page' => 4,
                'tax_query' => array(array(
                    'taxonomy' => 'product_cat',
                     'field'    => 'term_id',
                     'terms'     =>  $category_id,
                     'operator'  => 'IN'
                ))
                );
                $loop = new WP_Query($args);
                ?>
                <ul class="products">
                <?php
                while($loop->have_posts()){
                    $loop->the_post();
                    if($loop->post->ID != $product->get_id()){
                        wc_get_template_part( 'content', 'product' );
                    }
                }
                ?>
                </ul>
                <?php
        }

    ?> 



</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>