<?php

    if ( ! defined( 'ABSPATH' ) ) {
        exit;
    }

    do_action( 'woocommerce_before_account_navigation' );

    wp_nav_menu(array('theme_location' => 'account-menu')); 

    do_action( 'woocommerce_after_account_navigation' ); 

?>