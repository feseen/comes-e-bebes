<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$allowed_html = array(
	'a' => array(
		'href' => array(),
	),
);
?>

<p>
	<?php
	printf(
		/* translators: 1: user display name 2: logout url */
		wp_kses( __( 'Olá %1$s (não é %1$s? <a href="%2$s">Sair</a>)', 'woocommerce' ), $allowed_html ),
		esc_html( $current_user->display_name ),
		esc_url( wc_logout_url() )
	);
	?>
</p
>

<p>
	<?php
	/* translators: 1: Orders URL 2: Address URL 3: Account URL. */
	$dashboard_desc = __( 'Olá, tiamatcod (não é tiamatcod? Sair)

A partir do painel de controle de sua conta, você pode ver suas  <a href="%1$s">compras recentes</a>, gerenciar seus <a href="%2$s">endereços de entrega</a>, e <a href="%3$s">editar sua senha e detalhes da conta.</a>.', 'woocommerce' );
	if ( wc_shipping_enabled() ) {
		/* translators: 1: Orders URL 2: Addresses URL 3: Account URL. */
		$dashboard_desc = __( 'A partir do painel de controle de sua conta, você pode ver suas  <a href="%1$s">compras recentes</a>, gerenciar seus <a href="%2$s">endereços de entrega</a>, e <a href="%3$s">editar sua senha e detalhes da conta.</a>.', 'woocommerce' );
	}
	printf(
		wp_kses( $dashboard_desc, $allowed_html ),
		esc_url( wc_get_endpoint_url( 'orders' ) ),
		esc_url( wc_get_endpoint_url( 'edit-address' ) ),
		esc_url( wc_get_endpoint_url( 'edit-account' ) )
	);
	?>
</p>

<?php include('form-edit-account.php');  ?>

<?php
	/**
	 * My Account dashboard.
	 *
	 * @since 2.6.0
	 */
	do_action( 'woocommerce_account_dashboard' );

	/**
	 * Deprecated woocommerce_before_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_before_my_account' );

	/**
	 * Deprecated woocommerce_after_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_after_my_account' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
