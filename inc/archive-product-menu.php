<?php function create_price_filter(){ ?>
  <label>Filtro de preço:</label>
  <form class="filtro-preco" action="<?= $_SERVER['REQUEST_URI']; ?>">
    <div>
      <label for="min_price">De:</label>
      <input type="text" name="min_price" id="min_price" value="<?= $_GET['min_price']; ?>">
    </div>
    <div>
      <label for="max_price">Até:</label>
      <input type="text" name="max_price" id="max_price" value="<?= $_GET['max_price']; ?>">
    </div>
    <button type="submit" style="display:none;";>Filtrar</button>
  </form>
  
<?php } 

function categories_menu(){
    wp_nav_menu(array('theme_location' => 'categories-menu'));
}

function add_search_form(){ ?>
  
  <?php
	wc_get_template_part( 'product', 'searchform' );
}

function my_remove_product_result_count() { 
  remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_catalog_ordering' , 30);
  remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_result_count', 20 );
  remove_action( 'woocommerce_after_shop_loop' , 'woocommerce_result_count', 20 );
  remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar' , 10);
  remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
}

function category_title(){
  global $template;
  if(basename($template) == 'archive-product.php'){
    return;
  }
  ?>
  <h1>Pratos</h1>
  <h2>Comida <?php woocommerce_page_title(); ?></h2>
  <?php
}


function no_title(){
  return false;    
}

add_filter( 'woocommerce_show_page_title', 'no_title' );
add_action( 'after_setup_theme', 'my_remove_product_result_count');


?>