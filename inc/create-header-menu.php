<?php function create_header_menu(){ ?>
    <div id="header-container">
        <ul id="header-menu">
            <div id="home-search-bar">
                <li class="menu-item">
                    <a href="/home">
                        <img class="home-icon" src="<?php echo get_stylesheet_directory_uri() ?>/assets/home-icon.svg">
                    </a>
                </li>
                <li class="menu-item">
                    <div class="header-searchbar">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/lupa.png" class="img-searchbar">
                        <?php add_search_form(); ?>
                        
                    </div>
                </li>
            </div>
            <div id="orders-account">
                <li class="menu-item">
                    <button class="store-button"><a href="/shop">Faça seu pedido</a></button>
                </li>
                <li class="menu-item">
                    <a id="mini-cart">
                        <img class="cart-icon" src="<?php echo get_stylesheet_directory_uri() ?>/assets/cart-icon.svg">
                    </a>
                </li>
                <li class="menu-item">
                    <a href="/my-account">
                        <img class="account-icon" src="<?php echo get_stylesheet_directory_uri() ?>/assets/account-icon.svg">
                    </a>
                </li>
            </div>
        </ul>
    </div>
    <?php } ?>

<?php ?>