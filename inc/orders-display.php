<?php
    function custom_account_orders_columns() {
        $columns = array(
            'order-number'  => __( 'Order', 'woocommerce' ),
            'order-date'    => __( 'Date', 'woocommerce' ),
            'order-status'  => __( 'Status', 'woocommerce' ),
            'order-total'   => __( 'Total', 'woocommerce' ),
            'order-actions' => __( '', 'woocommerce' ),
        );

        return $columns;
    }

    add_filter( 'woocommerce_account_orders_columns', 'custom_account_orders_columns' );
?>