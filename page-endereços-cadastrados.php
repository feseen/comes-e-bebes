<?php
// Template Name: endereços cadastrados
?>
<?php get_header() ?>
    <main> 
        <div class='container-text'>
            <p>Os endereços a seguir serão usados na página de finalizar pedido como endereços padrões, mas é possível modificá-los durante a finalização do pedido
            </p> 
        </div>

        <div class='container endereços'>
            <div>
                <h3>Endereço de entrega</h3>
                <a href='page-novo-endereço.php'>Adicionar novo endereço</a>

            </div>
            <div>
                <?php 
                    while(have_posts()){
                        the_post();
                        the_content();
                    }
                ?>

            </div>  
        </div>

    </main> 

<?php get_footer() ?>